package com.example.alyssaangco.winapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Random;

public class  Bet extends AppCompatActivity {

    private EditText wallet;
    private EditText wager;
    private TextView message;
    private RadioGroup choice;
    private Button roll;
    private RadioButton even;
    private RadioButton odd;
    final Context context = this;
    private int counter = 0;
    private int counterWin = 0;
    private String call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bet);

        wallet = (EditText) findViewById(R.id.etWallet);
        wager = (EditText) findViewById(R.id.etWager);
        message = (TextView) findViewById(R.id.tvMessage);
        choice = (RadioGroup) findViewById(R.id.rgChoice);
        roll = (Button) findViewById(R.id.btnRoll);

        if (getIntent().getStringExtra("checked")!=null) {
             call = getIntent().getStringExtra("checked");
        }


        final int walletValue = getIntent().getIntExtra("value", 50000);

        wallet.setText("" + walletValue);
        wallet.setEnabled(false);

        roll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                counter++;
                double value = Double.parseDouble((wallet.getText().toString()));
                double wgr = Double.parseDouble((wager.getText().toString()));

                if (value <= 0 || wgr <= 0) {
                    Toast.makeText(Bet.this, "Wallet and wager must be greater than 0", Toast.LENGTH_SHORT).show();
                }
                else if (wgr > value){
                    Toast.makeText(Bet.this, "Wager must not be greater than wallet", Toast.LENGTH_SHORT).show();
                }

                else {
                    String result = "";
                    Random random = new Random();
                    int sum = (random.nextInt(6) + 1);
                    double remainder = sum%2;
                    double newWallet = 0;

                    if (remainder == 0){
                        switch (choice.getCheckedRadioButtonId()) {
                            case R.id.rbEven:
                                message.setText("The sum is " + sum + ". You WIN!");
                                newWallet = value + wgr;
                                wallet.setText(Double.toString(newWallet));
                                wallet.setFocusable(false);
                                wager.setHint("0");
                                wager.setText("");
                                counterWin++;
                                break;
                            case R.id.rbOdd:
                                message.setText(("The sum is " + sum + ". You LOSE!"));
                                newWallet = value - wgr;
                                wallet.setText(Double.toString(newWallet));
                                wallet.setFocusable(false);
                                wager.setHint("0");
                                wager.setText("");
                                break;
                        }
                    }
                    else {
                        switch (choice.getCheckedRadioButtonId()) {
                            case R.id.rbOdd:
                                message.setText(("The sum is " + sum + ". You WIN!"));
                                newWallet = value + wgr;
                                wallet.setText(Double.toString(newWallet));
                                wallet.setFocusable(false);
                                wager.setHint("0");
                                wager.setText("");
                                counterWin++;
                                break;
                            case R.id.rbEven:
                                message.setText(("The sum is " + sum + ". You LOSE!"));
                                newWallet = value - wgr;
                                wallet.setText(Double.toString(newWallet));
                                wallet.setFocusable(false);
                                wager.setHint("0");
                                wager.setText("");
                                break;
                        }
                    }
                    if (newWallet == 0){
                        even = (RadioButton) findViewById(R.id.rbEven);
                        odd = (RadioButton) findViewById(R.id.rbOdd);

                        Toast.makeText(Bet.this, "GAME OVER!", Toast.LENGTH_SHORT).show();
                        roll.setEnabled(false);
                        wallet.setEnabled(false);
                        even.setEnabled(false);
                        odd.setEnabled(false);
                        wager.setEnabled(false);

                        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                        alertDialogBuilder
                                .setTitle("GAME OVER!")
                                .setMessage("Do you wish to continue?")
                                .setPositiveButton("YES!", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Intent intent = new Intent(getApplicationContext(), Bet.class);
                                        startActivity(intent);
                                    }
                                })
                                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        System.exit(0);
                                    }
                                });
                        AlertDialog alertDialog = alertDialogBuilder.create();
                        alertDialog.show();
                    }

                }
                if(counter==3){
                    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                    AlertDialog.Builder builder = alertDialogBuilder
                            .setTitle("CONGRATULATIONS")
                            .setMessage("You and your chosen charity, " + call + " won! " + "You won " + counterWin + " time/s. " + "Do you want to restart the game?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), SplashScreen.class);
                                    startActivity(intent);
                                }

                            })

                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    System.exit(0);
                                }
                            });

                    AlertDialog alertDialog = alertDialogBuilder.create();
                    alertDialog.show();
                }
            }


        });
    }
}