package com.example.alyssaangco.winapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class PlayActivity extends AppCompatActivity {

    private Button a;
    private Button b;
    private Button c;

    private RadioButton cancer;
    private RadioButton rehab;
    private RadioButton home;
    private RadioButton women;

    private RadioGroup group;

    final int min = 5;
    final int max = 10;
    String str;

    Bundle bundle = new Bundle();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play);

        a = (Button) findViewById(R.id.btnA);
        b = (Button) findViewById(R.id.btnB);
        c = (Button) findViewById(R.id.btnC);

        group = (RadioGroup) findViewById(R.id.rdGroup);


        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                switch(group.getCheckedRadioButtonId()){
                    case R.id.rdCancer:
                        str = "Cancer Patients";
                        break;
                    case R.id.rdRehab:
                        str = "Rehabilitation Center";
                        break;
                    case R.id.rdHome:
                        str = "Home for the Aged";
                        break;
                    case R.id.rdWomen:
                        str = "Protection for Women";
                        break;
                }

                Intent intent = new Intent (getApplicationContext(), Bet.class);
                intent.putExtra("value",getRandomNumber(min,max)*10000);
               intent.putExtra("checked", str);
                startActivity(intent);
            }
        };

        a.setOnClickListener(listener);

        b.setOnClickListener(listener);

        c.setOnClickListener(listener);

    }
    private int getRandomNumber(int min,int max) {
        return (new Random()).nextInt((max - min) + 1) + min;
    }



}
